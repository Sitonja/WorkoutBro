package com.example.sito.workoutbroapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sito.workoutbroapplication.MainActivity;
import com.example.sito.workoutbroapplication.R;
import com.example.sito.workoutbroapplication.domain.CurrentState;
import com.example.sito.workoutbroapplication.domain.TrainingHelper;
import com.example.sito.workoutbroapplication.domain.Workout;

import android.content.Intent;


/**
 * Created by Sito on 13.5.2017..
 */

public class ExercisesAdapter extends ArrayAdapter<String>{
    private Workout work;

    public ExercisesAdapter(Context context, Workout w){
        super(context, R.layout.program_list_item, TrainingHelper.getNames(w));
        work = w;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater exerciseInflater = LayoutInflater.from(getContext());
        View exerciseView = exerciseInflater.inflate(R.layout.program_list_item, parent, false);




        TextView name = (TextView)exerciseView.findViewById(R.id.name);
        TextView reps = (TextView)exerciseView.findViewById(R.id.reps);
        TextView rest = (TextView)exerciseView.findViewById(R.id.rest);

        name.setText(TrainingHelper.getNames(work)[position]);
        rest.setText(String.valueOf("Rest: " + TrainingHelper.getRest(work)[position] + " sec"));
        if(TrainingHelper.getCounts(work)[position]!=0) {
            reps.setText(String.valueOf("Reps: " + TrainingHelper.getCounts(work)[position] + " x " + TrainingHelper.getReps(work)[position]));
        }else{
            reps.setText(String.valueOf("Reps: max"+ " x " + TrainingHelper.getReps(work)[position]));
        }


            return exerciseView;
    }
}
