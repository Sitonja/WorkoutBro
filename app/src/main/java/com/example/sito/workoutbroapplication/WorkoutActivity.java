package com.example.sito.workoutbroapplication;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sito.workoutbroapplication.domain.CountDownTimerWithPause;
import com.example.sito.workoutbroapplication.domain.CurrentState;
import com.example.sito.workoutbroapplication.domain.Exercises;
import com.example.sito.workoutbroapplication.domain.HistoryRecord;
import com.example.sito.workoutbroapplication.domain.TrainingHelper;
import com.example.sito.workoutbroapplication.domain.Workout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Random;


public class WorkoutActivity extends Activity {

    CurrentState currentState;
    int level;
    int restTime;
    int[] counts;

    //
    String[] names;
    int rank;
    int[] reps;
    int testReps;
    TextView repCount;

    int[] sounds;
    //
    boolean changeRank;

    int currentCount;
    int countdownState = 1; // 1 - in progress 2 - countdown running 3 - countdown paused
    CountDownTimerWithPause countDownTimer;
    //ProgressBar progressBar;
    TextView timerText;
    //TextView countText;
    Button doneButton;
    Button skipButton;
    TextView descriptionText;
    TextView instructionsText;
    LinearLayout countsList;
    LayoutInflater inflater;
    ImageView workoutImage;
    //ImageView pushupsImage;
    //ImageView squatsImage;
    //ImageView situpsImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        countdownState = 1;

        currentState = TrainingHelper.getCurrentState(this);
        level = currentState.level;
        if(level == 0)
            finish();

        inflater = LayoutInflater.from(this);

        descriptionText = (TextView)findViewById(R.id.w_description_label);
        instructionsText = (TextView)findViewById(R.id.w_instructions_label);
        doneButton = (Button)findViewById(R.id.w_done_button);
        skipButton = (Button)findViewById(R.id.w_skip_button);
        //countText = (TextView)findViewById(R.id.w_count);
        timerText = (TextView)findViewById(R.id.w_timer);
        //progressBar = (ProgressBar)findViewById(R.id.w_timer_progress);
        countsList = (LinearLayout)findViewById(R.id.w_count_list);

        workoutImage = (ImageView)findViewById(R.id.workout_image);
        //pushupsImage = (ImageView)findViewById(R.id.w_pushups);
        //squatsImage = (ImageView)findViewById(R.id.w_squats);
        //situpsImage = (ImageView)findViewById(R.id.w_situps);

        //
        repCount = (TextView)findViewById(R.id.w_rep_count);

        if(getIntent().getExtras()!= null){
            changeRank = false;
            rank = getIntent().getExtras().getInt("rank");
        }else{
            changeRank = true;
            rank = currentState.currentRank;
        }
        descriptionText.setText(String.valueOf("Rank: " + rank));
        //
        Workout w = TrainingHelper.getWorkout(rank-1);
        if(w==null) w = TrainingHelper.getWorkout(0);
        counts = TrainingHelper.getCounts(w);

        //
//        names = TrainingHelper.getNames(w);
        names = TrainingHelper.getNames(w);
        reps = TrainingHelper.getReps(w);
        testReps = 0;
        //
        restTime = w.restTime;


        sounds= new int[]{R.raw.ajmo, R.raw.ajdejedanpojedan, R.raw.dragi_prijatelju, R.raw.fejstufejs,R.raw.i_jedno_i_drugo_i_trece,R.raw.ja_znam_i_spreman_sam_se_s_time_nosit,
                R.raw.jakaemokdecki,R.raw.jasamsposobancovjek, R.raw.sljedece, R.raw.sve_cu_vas_pobjedit, R.raw.sve_u_redu};

        startCount();

    }

    public void onDoneClick(View v){ //kada stisnemo done


        if(countdownState == 1)
        {
            if(currentCount >= (counts.length - 1) && testReps >= (reps[currentCount]-1)) //kad je gotovo
            {
                long milis = TrainingHelper.getCurrentTime();
                currentState = TrainingHelper.getCurrentState(this);
                currentState.lastWorkoutCompletionTime = milis;
                //
//                if(changeRank){
//                    currentState.currentRank = rank+1;
//                }
                //
//                Workout nextWorkout = TrainingHelper.getNextWorkout(week, day);
                Workout nextWorkout = TrainingHelper.getWorkout(rank);
                Workout w = TrainingHelper.getWorkout(rank-1);
                if(nextWorkout == null && getIntent().getExtras()== null) {
                    currentState.isFinished = true;
                }



                HistoryRecord hr = new HistoryRecord();
                hr.counts = counts;
                hr.rank = rank;
                hr.completionTime = milis;
                //
                hr.reps = reps;
                hr.names = names;
                hr.exerciseRest = TrainingHelper.getExerciseRest(w);
                hr.restTime = TrainingHelper.getSetRest(w);
                //
                currentState.historyRecords.add(hr);

                TrainingHelper.saveCurrentState(this, currentState);

                Intent intent = new Intent(this, CongratulationsActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.putExtra("changeRank", changeRank);
                startActivity(intent);
            }else{
                Workout w = TrainingHelper.getWorkout(rank-1);

                if(testReps <  reps[currentCount]-1){
                    testReps++;
                    startTimer(TrainingHelper.getExerciseRest(w)[currentCount]);
                }else{
                    currentCount ++;
                    startTimer(restTime); //inace pokreni timer
                    testReps = 0;
                }
            }
        }
        else if(countdownState == 2){ //kad je u pauzi
            countdownState = 3;
            countDownTimer.pause();
            doneButton.setText(getString(R.string.resume));
        }
        else if(countdownState == 3){ //kad nije pauza
            countdownState = 2;
            countDownTimer.resume();
            doneButton.setText(getString(R.string.pause));
        }
    }

    public void onSkipClick(View v){
        //currentCount ++;
        startCount();
        countDownTimer.cancel();
    }

    private void startCount(){ //prije nego stisnemo done


        Random r = new Random();
        int Low = 0;
        int High = sounds.length;
        int rndm = r.nextInt(High-Low);
        int probability = r.nextInt(3);

        if(probability==0) {
            final MediaPlayer mp1 = MediaPlayer.create(getApplicationContext(), sounds[rndm]);
            mp1.start();
        }
        countdownState = 1;
        //countText.setVisibility(View.VISIBLE);
        doneButton.setVisibility(View.VISIBLE);
        skipButton.setVisibility(View.GONE);
        doneButton.setText(getString(R.string.done));
        //progressBar.setVisibility(View.GONE);
        timerText.setVisibility(View.GONE);



        workoutImage.setVisibility(View.VISIBLE);
        //pushupsImage.setVisibility(View.VISIBLE);
        //squatsImage.setVisibility(View.VISIBLE);
        //situpsImage.setVisibility(View.VISIBLE);
        repCount.setVisibility(View.VISIBLE);
        String repetitionsCount = String.valueOf(counts[currentCount]);

        repCount.setText((testReps+1) + " / " + reps[currentCount]);

        if(names!=null) {
            if(counts[currentCount] != 0) {
                //countText.setText(repetitionsCount);
                instructionsText.setText(String.format("%1$s %2$s", repetitionsCount, names[currentCount]));
            }else{
                //countText.setText(String.valueOf("m"));
                instructionsText.setText(String.format("%1$s %2$s", "max", names[currentCount]));
            }


            switch (names[currentCount]) {
                case "Push-ups":
                    workoutImage.setImageResource(R.drawable.pushups);
                    break;
                case "Close grip push-ups":
                    workoutImage.setImageResource(R.drawable.close_grip_pushups);
                    break;
                case "Dips":
                    workoutImage.setImageResource(R.drawable.dips);
                    break;
                case "Chin-ups":
                    workoutImage.setImageResource(R.drawable.chinups);
                    break;
                case "Squats":
                    workoutImage.setImageResource(R.drawable.squats);
                    break;
                case "Sit-ups":
                    workoutImage.setImageResource(R.drawable.situps);
                    break;
                case "Pull-ups":
                    workoutImage.setImageResource(R.drawable.pullups);
                    break;
                case "Wide grip push-ups":
                    workoutImage.setImageResource(R.drawable.wide_pushups);
                    break;
                case "Decline push-ups":
                    workoutImage.setImageResource(R.drawable.decline_pushups);
                    break;
                case "Incline push-ups":
                    workoutImage.setImageResource(R.drawable.pushups);
                    break;
                case "Pseudo push-ups":
                    workoutImage.setImageResource(R.drawable.pseudo_pushups);
                    break;
                case "Clap push-ups":
                    workoutImage.setImageResource(R.drawable.clap_pushups);
                    break;
                case "Shoulder tap push-ups":
                    workoutImage.setImageResource(R.drawable.shoulder_tap_pushups);
                    break;
                case "Diamond push-ups":
                    workoutImage.setImageResource(R.drawable.diamond_pushups);
                    break;
                case "Pike push-ups":
                    workoutImage.setImageResource(R.drawable.pike);
                    break;
                case "Lunges":
                    workoutImage.setImageResource(R.drawable.lunges); //
                    break;
                case "Jump squats":
                    workoutImage.setImageResource(R.drawable.jumping_squats);
                    break;
                case "Wide grip pull-ups":
                    workoutImage.setImageResource(R.drawable.pullups);
                    break;
                case "Bicycle crunch":
                    workoutImage.setImageResource(R.drawable.bycicle);
                    break;
                case "Archer push-ups":
                    workoutImage.setImageResource(R.drawable.archer);
                    break;
                case "V-ups":
                    workoutImage.setImageResource(R.drawable.v_ups);
                    break;
                case "Plank push-ups":
                    workoutImage.setImageResource(R.drawable.plank_pushups);
                    break;
                case "Typewriter pushups":
                    workoutImage.setImageResource(R.drawable.archer);
                    break;
                case "Jump lunges":
                    workoutImage.setImageResource(R.drawable.jump_lunges);
                    break;
                default:
                    break;

            }
        }

        createCountsView();
    }

    private void createCountsView() {
        countsList.removeAllViews();

        for(int i = 0; i<counts.length; i++){
            int res = currentCount == i ? R.layout.workout_count_current : R.layout.workout_count;
            View row = inflater.inflate(res,null);
            TextView count = (TextView)row.findViewById(R.id.w_count_value);
            if(counts[i] != 0) {
                count.setText(String.valueOf(counts[i]));
            }else{
                count.setText("M");
            }
            countsList.addView(row);
            row = inflater.inflate(R.layout.workout_count, null);
            count = (TextView)row.findViewById(R.id.w_count_value);
            count.setText(String.valueOf("|"));
            countsList.addView(row);
        }

    }

    private void startTimer(final int sec) { //kad je stoperica pokrenuta
        countdownState = 2;
        //countText.setVisibility(View.GONE);
        //doneButton.setVisibility(View.GONE);
        doneButton.setText(getString(R.string.pause));
        skipButton.setVisibility(View.VISIBLE);
        //progressBar.setVisibility(View.VISIBLE);
        timerText.setVisibility(View.VISIBLE);
        timerText.setText(String.valueOf(sec));


        workoutImage.setVisibility(View.GONE);
        //pushupsImage.setVisibility(View.GONE);
        //squatsImage.setVisibility(View.GONE);
        //situpsImage.setVisibility(View.GONE);

        instructionsText.setText(getString(R.string.take_break_instructions));
       // progressBar.setMax(sec);
       // progressBar.setProgress(progressBar.getMax());
        countDownTimer = new CountDownTimerWithPause(sec * 1000, 1000, true) {
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                int seconds = (int)leftTimeInMilliseconds / 1000;
               // progressBar.setProgress(seconds);
                timerText.setText(String.valueOf(seconds));
            }
            @Override
            public void onFinish() { //kad je gotovo povecaj broj zavrsneih vjezbi
                //currentCount ++;
                startCount();
            }
        };

        countDownTimer.create();
    }
}
