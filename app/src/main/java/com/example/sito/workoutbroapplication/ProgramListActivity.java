package com.example.sito.workoutbroapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.example.sito.workoutbroapplication.adapter.ProgramListAdapter;
import com.example.sito.workoutbroapplication.adapter.ProgressAdapter;
import com.example.sito.workoutbroapplication.domain.CurrentState;
import com.example.sito.workoutbroapplication.domain.TrainingHelper;
import com.example.sito.workoutbroapplication.domain.Workout;

import java.lang.reflect.Array;

import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class ProgramListActivity extends Activity {

    ExpandableStickyListHeadersListView programList;
    StickyListHeadersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_list);

        programList = (ExpandableStickyListHeadersListView)findViewById(R.id.programs_list);

        adapter = new ProgramListAdapter(this, TrainingHelper.workouts);
        programList.setAdapter(adapter);

        programList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), WorkoutActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("rank", (position + 1));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
    }


}
