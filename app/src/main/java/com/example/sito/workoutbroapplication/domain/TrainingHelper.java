package com.example.sito.workoutbroapplication.domain;

import android.app.Activity;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.example.sito.workoutbroapplication.domain.ExerciseNames.*;

/**
 * Created by Maksym on 11/14/2014.
 */
public class TrainingHelper {
    public static Workout[] workouts = new Workout[]
            {
                    new Workout() {{
                        restTime = 120;

                        name = new String[]{WIDE_GRIP_PUSHUPS.toString(), DIPS.toString(), PULLUPS.toString(), SQUATS.toString(), SIT_UPS.toString()};
                        counts = new int[]{8,8,5,8,8};

                        exerciseRest = new int[]{ 30,30,30,30,30};
                        reps = new int[]{2,2,2,3,3};
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), PUSHUPS.toString(), PULLUPS.toString(), SQUATS.toString(), SIT_UPS.toString()};
                        restTime = 120;
                        counts = new int[]{5,10,5,8,8};

                        exerciseRest = new int[]{ 30,30,30,30,30};

                        reps = new int[]{4,3,3,3,3};
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), INCLINE_PUSHUPS.toString(), DECLINE_PUSHUPS.toString(), PULLUPS.toString(), SQUATS.toString(), SIT_UPS.toString()};
                        restTime = 180;
                        counts = new int[]{5,5,5,5,5,5};

                        exerciseRest = new int[]{ 25,25,25,25,25,25};
                        reps = new int[]{6,6,6,4,6, 6};
                    }},
                    new Workout() {{
                        name = new String[]{PUSHUPS.toString(), CLOSE_GRIP_PUSHUPS.toString(), DIPS.toString(), SQUATS.toString(), SIT_UPS.toString()};
                        restTime = 120;
                        counts = new int[]{4,5,4,5,6};
                        reps = new int[]{6,6,6,3,6};
                        exerciseRest = new int[]{ 25,25,25,25,25,25};
                    }},
                    new Workout() {{
                        restTime = 120;
                        name = new String[]{DIPS.toString(), DIAMOND_PUSHUPS.toString(), INCLINE_PUSHUPS.toString(), PULLUPS.toString(), SQUATS.toString()};
                        counts = new int[]{3,2,5,5,5};
                        reps = new int[]{6,6,6,6,4};
                        exerciseRest = new int[]{ 25,25,25,25,25,25};
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), PUSHUPS.toString(), PULLUPS.toString(), SQUATS.toString()};
                        restTime = 180;
                        counts = new int[]{0,0,0,0};
                        reps = new int[]{2,2,2,2};
                        exerciseRest = new int[]{ 120,120,120,120};
                        //tu sam stao
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), INCLINE_PUSHUPS.toString(), DECLINE_PUSHUPS.toString(), WIDE_GRIP_PULLUPS.toString(), BICYCLE_CRUNCH.toString()};
                        restTime = 180;
                        counts = new int[]{8,4,8,4,30};
                        reps = new int[]{6,6,6,6, 1};
                        exerciseRest = new int[]{ 25,25,25,25,25};
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), INCLINE_PUSHUPS.toString(), WIDE_GRIP_PULLUPS.toString(), PULLUPS.toString(), ARCHER_PUSHUPS.toString()};
                        restTime = 180;
                        counts = new int[]{5,8,5,4, 0};
                        reps = new int[]{6,6,6,6, 1};
                        exerciseRest = new int[]{ 25,25,25,25,25};
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), DECLINE_PUSHUPS.toString(), PUSHUPS.toString(), WIDE_GRIP_PUSHUPS.toString()
                        , V_UPS.toString(), ARCHER_PUSHUPS.toString(), PLANK_PUSHUPS.toString()};
                        restTime = 120;
                        counts = new int[]{5,5,5,5, 10,5,5};
                        reps = new int[]{6,6,3,3, 3,3,2};
                        exerciseRest = new int[]{ 25,25,25,25,25, 25,25};
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), PULLUPS.toString(), PUSHUPS.toString(), SIT_UPS.toString()
                                , JUMP_SQUATS.toString()};
                        restTime = 180;
                        counts = new int[]{0,0,0,0,0};
                        reps = new int[]{3,4,4,4,4};
                        exerciseRest = new int[]{ 25,25,25,25,25};
                    }},
                    new Workout() {{
                        name = new String[]{DIPS.toString(), DECLINE_PUSHUPS.toString(), PUSHUPS.toString(), SIT_UPS.toString()
                                , TYPEWRITER_PUSHUPS.toString(), PLANK_PUSHUPS.toString(), JUMP_LUNGES.toString(), PULLUPS.toString()};
                        restTime = 180;
                        counts = new int[]{0,10,10,0,0,0,0,0};
                        reps = new int[]{5,6,3,6,2,4,4,2};
                        exerciseRest = new int[]{ 30,30,30,30,30,30,30,30};
                    }}
            };

    public static Workout getWorkout(int index){
        if(index >= 0 && index < workouts.length){
            return workouts[index];
        }
        return null;
    }

    public static int[] getReps(Workout w){
     return w.reps;
    }

    public static int[] getExerciseRest(Workout w){
        return w.exerciseRest;
    }

//    public static Workout getWorkout(int key){
//        for(Workout w : workouts){
//            if(getWorkoutKey(w) == key)
//                return w;
//        }
//
//        return null;
//    }


    public static int getWorkoutKey(int week, int day){
        return Integer.valueOf(String.valueOf(week)+String.valueOf(day));
    }


    /*public static int getNextWorkoutKey(int key){
        Workout w = getNextWorkout(key);
        if(w == null)
            return 0;

        return getWorkoutKey(w);
    }*/

    public static String getLevelDescription(int level){
        switch (level){
            case 1:
                return "Easy";
            case 2:
                return "Average";
            case 3:
                return "Above Average";
        }

        return "";
    }

    public static int[] getCounts(Workout w){
                return w.counts;
    }

    public static String[] getNames(Workout w){
                return w.name;
    }
    public static int[] getRest(Workout w){
                return w.exerciseRest;
    }

    public static int getSetRest(Workout w){
                return w.restTime;
    }



    public static CurrentState getCurrentState(Activity context){
        SharedPreferences mPrefs = context.getSharedPreferences("200", Activity.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("CurrentState", null);
        if(json == null)
            return new CurrentState();
        CurrentState obj = gson.fromJson(json, CurrentState.class);
        return obj;
    }

    public static void saveCurrentState(Activity context, CurrentState currentState){
        SharedPreferences mPrefs = context.getSharedPreferences("200", Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        String json = null;
        Gson gson = new Gson();
        json = gson.toJson(currentState);
        prefsEditor.putString("CurrentState", json);
        prefsEditor.commit();
    }

    public static long getCurrentTime(){
        Calendar c = Calendar.getInstance();
        return c.getTimeInMillis();
    }

    public static String getWorkoutDescription(int level, int rank) {
        //Workout w = getWorkout(getWorkoutKey(week, day));
        Workout w = getWorkout(rank-1);
        //return String.format("Week %d/Day %d (%s). Rank %d", w.week, w.day, join(TrainingHelper.getCounts(w, level), ","), rank);
        return String.format("(%s). Rank %d",join(TrainingHelper.getCounts(w), ","), rank);
    }

    public static String join(int r[],String d)
    {
        if (r.length == 0) return "";
        StringBuilder sb = new StringBuilder();
        int i;
        for(i=0;i<r.length-1;i++)
            sb.append(String.valueOf(r[i])+d);
        return sb.toString()+ String.valueOf(r[i]);
    }

    public static String getDateString(long milliSeconds)
    {
        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        return formatter.format(calendar.getTime());
    }
}