package com.example.sito.workoutbroapplication;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;




import com.example.sito.workoutbroapplication.adapter.DescriptionAdapter;
import com.example.sito.workoutbroapplication.domain.ExerciseNames;

import java.util.Arrays;
import java.util.List;

import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class InstructionsActivity extends Activity {

    ExpandableStickyListHeadersListView descriptionList;
    //Spinner levelSpinner;
    StickyListHeadersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);


        descriptionList = (ExpandableStickyListHeadersListView )findViewById(R.id.workout_description_list);



        adapter = new DescriptionAdapter(this, ExerciseNames.getEnumList());
        descriptionList.setAdapter(adapter);






        // Font path
        String fontPath = "fonts/RacingSansOne-Regular.ttf";

        TextView txtInstructions = (TextView) findViewById(R.id.indroductionTitle);

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

        // Applying font
        txtInstructions.setTypeface(tf);
    }
}
