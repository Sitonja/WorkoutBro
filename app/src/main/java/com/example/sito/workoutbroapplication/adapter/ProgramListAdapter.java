package com.example.sito.workoutbroapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sito.workoutbroapplication.R;
import com.example.sito.workoutbroapplication.domain.Workout;


import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class ProgramListAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private LayoutInflater inflater;
    private Workout[] workouts;

    public ProgramListAdapter(Context context, Workout[] workouts){
        inflater = LayoutInflater.from(context);
        this.workouts = workouts;
    }

    @Override
    public int getCount() {
        return workouts.length;
    }

    @Override
    public Object getItem(int position) {
        return workouts[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    /*
    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.program_list_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.program_header_text);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText = "Rank: " + String.valueOf(position +1);
        holder.text.setText(headerText);
        return convertView;
    }
    */

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        return new View(parent.getContext());
    }

    @Override
    public long getHeaderId(int position) {
        return (position+1);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.plan_list_item, parent, false);
            holder.repetitions = (TextView) convertView.findViewById(R.id.program_repetitions_text);
            holder.text = (TextView) convertView.findViewById(R.id.program_header_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Workout w = workouts[position];
        String s="Rest time: " + w.restTime + "\n";
        for (int i=0;i<w.name.length;++i) {
            if(w.counts[i]!= 0) {
                s += "" + w.counts[i] + " x " + w.reps[i] + " " + w.name[i] + "| Rest: "+ w.exerciseRest[i] + "\n";
            }else{
                s += "max x " + w.reps[i] + " " + w.name[i] + " | Rest: "+ w.exerciseRest[i] + "\n";
            }
        }
        //holder.repetitions.setText(String.format("%s", TrainingHelper.join(w.counts, "/")));
        holder.repetitions.setText(s);

        String headerText = "Rank: " + String.valueOf(position +1);
        holder.text.setText(headerText);

        return convertView;
    }


    class ViewHolder {

        TextView repetitions;
        TextView text;
    }
}
