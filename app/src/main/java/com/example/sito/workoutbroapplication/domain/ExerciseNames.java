package com.example.sito.workoutbroapplication.domain;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Sito on 10.5.2017..
 */

public enum ExerciseNames implements Comparable<ExerciseNames>{
    PUSHUPS("Push-ups"), CLOSE_GRIP_PUSHUPS("Close grip push-ups"), DIPS("Dips"), CHIN_UPS("Chin-ups"), SQUATS("Squats"),
    SIT_UPS("Sit-ups"), PULLUPS("Pull-ups"), WIDE_GRIP_PUSHUPS("Wide grip push-ups"), DECLINE_PUSHUPS("Decline push-ups"),
    INCLINE_PUSHUPS("Incline push-ups"), PSEUDO_PUSHUPS("Pseudo push-ups"), CLAP_PUSHUPS("Clap push-ups"), SHOULDER_TAP_PUSHUPS("Shoulder tap push-ups"),
    DIAMOND_PUSHUPS("Diamond push-ups"), PIKE_PUSHUPS("Pike push-ups"), LUNGES("Lunges"), JUMP_SQUATS("Jump squats"),
    WIDE_GRIP_PULLUPS("Wide grip pull-ups"), BICYCLE_CRUNCH("Bicycle crunch"), ARCHER_PUSHUPS("Archer push-ups"), V_UPS("V-ups"),
    PLANK_PUSHUPS("Plank push-ups"), TYPEWRITER_PUSHUPS("Typewriter pushups"), JUMP_LUNGES("Jump lunges");

    private final String text;

    private ExerciseNames(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

    public static List<ExerciseNames> getEnumList() {

        List<ExerciseNames> list=Arrays.asList(values());

        return list;
    }





}
