package com.example.sito.workoutbroapplication;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import com.example.sito.workoutbroapplication.adapter.ExercisesAdapter;
import com.example.sito.workoutbroapplication.domain.TrainingHelper;
import com.example.sito.workoutbroapplication.domain.Workout;

public class ProgramActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);

        Bundle extras = getIntent().getExtras();
        int rank = 0;
        if (extras != null) {
            rank = extras.getInt("rank");
        }
        Workout w = TrainingHelper.getWorkout(rank-1);
        String[] exercises = TrainingHelper.getNames(w);
//        String[] exercises = {"push", "pull", "squat"};
        ListAdapter exercisesAdapter = new ExercisesAdapter(this, w);
        ListView exerciseListView = (ListView) findViewById(R.id.exercises_list);
        exerciseListView.setAdapter(exercisesAdapter);

        TextView programHeader = (TextView)findViewById(R.id.program_header);
        TextView setRest =(TextView)findViewById(R.id.set_rest);

        programHeader.setText(String.valueOf("Rank: " + rank));
        setRest.setText(String.valueOf("Set rest: " + TrainingHelper.getSetRest(w) + " sec"));
    }

    public void onStartClick(View v){
//        final MediaPlayer ajmoSoundMediaPlayer = MediaPlayer.create(this, R.raw.ajmo);
//
//        ajmoSoundMediaPlayer.start();

        Intent intent = new Intent(this, WorkoutActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);



    }
}
