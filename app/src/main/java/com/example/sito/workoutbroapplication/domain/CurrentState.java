package com.example.sito.workoutbroapplication.domain;

import java.util.ArrayList;
import java.util.List;


public class CurrentState {
    public Boolean isFinished;
    public long startTime;
    public int level;
    public long lastWorkoutCompletionTime;

    public int currentRank; //razina vjezbe

    public List<HistoryRecord> historyRecords;

    public CurrentState(){
        historyRecords = new ArrayList<HistoryRecord>();
        isFinished = false;
    }
}
