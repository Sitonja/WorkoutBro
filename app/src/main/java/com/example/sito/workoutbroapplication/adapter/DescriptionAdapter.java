package com.example.sito.workoutbroapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sito.workoutbroapplication.R;
import com.example.sito.workoutbroapplication.domain.ExerciseNames;
import com.example.sito.workoutbroapplication.domain.Exercises;
import com.example.sito.workoutbroapplication.domain.HistoryRecord;
import com.example.sito.workoutbroapplication.domain.Level;
import com.example.sito.workoutbroapplication.domain.TrainingHelper;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by jprim on 12-May-17.
 */

public class DescriptionAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private LayoutInflater inflater;
    private List<ExerciseNames> vjezbe;

    public DescriptionAdapter(Context context, List<ExerciseNames> vjezbeList){
        inflater = LayoutInflater.from(context);
        vjezbe = vjezbeList;

        Collections.sort(vjezbe, new Comparator<ExerciseNames>() {
            @Override
            public int compare(ExerciseNames o1, ExerciseNames o2) {
                return o1.toString().compareTo(o2.toString());
            }

        });
    }

    @Override
    public int getCount() {
        return vjezbe.size();
    }

    @Override
    public Object getItem(int position) {
        return vjezbe.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        DescriptionAdapter.HeaderViewHolder holder;
        if (convertView == null) {
            holder = new DescriptionAdapter.HeaderViewHolder();
            convertView = inflater.inflate(R.layout.description_list_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.description_header_text);
            convertView.setTag(holder);
        } else {
            holder = (DescriptionAdapter.HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText = vjezbe.get(position).toString();
        holder.text.setText(headerText);
        return convertView;
    }
    */

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        return new View(parent.getContext());
    }


    @Override
    public long getHeaderId(int position) {
        return vjezbe.get(position).toString().hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DescriptionAdapter.ViewHolder holder;


        if (convertView == null) {
            holder = new DescriptionAdapter.ViewHolder();

            convertView = inflater.inflate(R.layout.description_list_item, parent, false);
            holder.description= (TextView) convertView.findViewById(R.id.description_text); //description
            holder.header=(TextView) convertView.findViewById(R.id.description_header_text);
            holder.workoutImage=(ImageView) convertView.findViewById(R.id.workout_image);
            convertView.setTag(holder);
        } else {
            holder = (DescriptionAdapter.ViewHolder) convertView.getTag();
        }

        ExerciseNames n = vjezbe.get(position);
       // holder.day.setText(String.format("Day %d", w.day));
        //holder.repetitions.setText(String.format("%s", TrainingHelper.join(w.counts, "/")));
        String desc = Exercises.getDescription(n.toString());

        holder.description.setText(desc);

        String headerText = vjezbe.get(position).toString();
        holder.header.setText(headerText);

        switch (n.toString()) {
            case "Push-ups":
                holder.workoutImage.setImageResource(R.drawable.pushups);
                break;
            case "Close grip push-ups":
                holder.workoutImage.setImageResource(R.drawable.close_grip_pushups);
                break;
            case "Dips":
                holder.workoutImage.setImageResource(R.drawable.dips);
                break;
            case "Chin-ups":
                holder.workoutImage.setImageResource(R.drawable.chinups);
                break;
            case "Squats":
                holder.workoutImage.setImageResource(R.drawable.squats);
                break;
            case "Sit-ups":
                holder.workoutImage.setImageResource(R.drawable.situps);
                break;
            case "Pull-ups":
                holder.workoutImage.setImageResource(R.drawable.pullups);
                break;
            case "Wide grip push-ups":
                holder.workoutImage.setImageResource(R.drawable.wide_pushups);
                break;
            case "Decline push-ups":
                holder.workoutImage.setImageResource(R.drawable.decline_pushups);
                break;
            case "Incline push-ups":
                holder.workoutImage.setImageResource(R.drawable.pushups);
                break;
            case "Pseudo push-ups":
                holder.workoutImage.setImageResource(R.drawable.pseudo_pushups);
                break;
            case "Clap push-ups":
                holder.workoutImage.setImageResource(R.drawable.clap_pushups);
                break;
            case "Shoulder tap push-ups":
                holder.workoutImage.setImageResource(R.drawable.shoulder_tap_pushups);
                break;
            case "Diamond push-ups":
                holder.workoutImage.setImageResource(R.drawable.diamond_pushups);
                break;
            case "Pike push-ups":
                holder.workoutImage.setImageResource(R.drawable.pike);
                break;
            case "Lunges":
                holder.workoutImage.setImageResource(R.drawable.lunges); //
                break;
            case "Jump squats":
                holder.workoutImage.setImageResource(R.drawable.jumping_squats);
                break;
            case "Wide grip pull-ups":
                holder.workoutImage.setImageResource(R.drawable.pullups);
                break;
            case "Bicycle crunch":
                holder.workoutImage.setImageResource(R.drawable.bycicle);
                break;
            case "Archer push-ups":
                holder.workoutImage.setImageResource(R.drawable.archer);
                break;
            case "V-ups":
                holder.workoutImage.setImageResource(R.drawable.v_ups);
                break;
            case "Plank push-ups":
                holder.workoutImage.setImageResource(R.drawable.plank_pushups);
                break;
            case "Typewriter pushups":
                holder.workoutImage.setImageResource(R.drawable.archer);
                break;
            case "Jump lunges":
                holder.workoutImage.setImageResource(R.drawable.jump_lunges);
                break;
            default:
                break;

        }




        return convertView;
    }


    class ViewHolder {
        TextView description;
        TextView header;
        ImageView workoutImage;
    }
}
