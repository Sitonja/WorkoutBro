package com.example.sito.workoutbroapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.sito.workoutbroapplication.domain.CurrentState;
import com.example.sito.workoutbroapplication.domain.TrainingHelper;


public class CongratulationsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);

        Button yesButton = (Button) findViewById(R.id.w_yes_button);
        Button noButton = (Button) findViewById(R.id.w_no_button);
        Button doneButton = (Button) findViewById(R.id.w_done_button);
        TextView description = (TextView) findViewById(R.id.c_description_label);

        if( ! getIntent().getExtras().getBoolean("changeRank")){

            doneButton.setVisibility(View.VISIBLE);
            yesButton.setVisibility(View.GONE);
            noButton.setVisibility(View.GONE);
            description.setText(getString(R.string.workout_finished2));

        }else{
            doneButton.setVisibility(View.GONE);
            yesButton.setVisibility(View.VISIBLE);
            noButton.setVisibility(View.VISIBLE);

            description.setText(getString(R.string.workout_finished));

        }


        // Font path
        String fontPath = "fonts/RacingSansOne-Regular.ttf";

        TextView header = (TextView ) findViewById(R.id.congratulationsTitle);

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

        // Applying font
        header.setTypeface(tf);
    }

    public void onYesClick(View v){
        final MediaPlayer ajmoSoundMediaPlayer = MediaPlayer.create(this, R.raw.apsolutno_da);
        ajmoSoundMediaPlayer.start();

        CurrentState currentState = TrainingHelper.getCurrentState(this);
        currentState.currentRank++;
        TrainingHelper.saveCurrentState(this, currentState);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onNoClick(View v){
        final MediaPlayer ajmoSoundMediaPlayer = MediaPlayer.create(this, R.raw.ne_vredi);
        ajmoSoundMediaPlayer.start();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onDoneClick(View v){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
