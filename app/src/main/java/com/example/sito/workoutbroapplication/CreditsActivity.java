package com.example.sito.workoutbroapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class CreditsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);

        // Font path
        String fontPath = "fonts/RacingSansOne-Regular.ttf";

        // text view label
        TextView txtSito = (TextView) findViewById(R.id.Sito);
        TextView txtPrimorac = (TextView) findViewById(R.id.Primorac);
        TextView txtBoban = (TextView) findViewById(R.id.Boban);

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

        // Applying font
        txtSito.setTypeface(tf);
        txtPrimorac.setTypeface(tf);
        txtBoban.setTypeface(tf);
    }


}
