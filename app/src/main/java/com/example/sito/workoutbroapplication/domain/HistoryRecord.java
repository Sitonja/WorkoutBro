package com.example.sito.workoutbroapplication.domain;

/**
 * Created by Maksym on 11/15/2014.
 */
public class HistoryRecord {
    public int rank;
    public long completionTime;
    public int[] counts;
    public int[] reps;
    public int[] exerciseRest;
    public int restTime;
    public String[] names;
}
