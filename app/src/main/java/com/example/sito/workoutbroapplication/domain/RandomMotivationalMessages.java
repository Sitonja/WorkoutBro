package com.example.sito.workoutbroapplication.domain;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/**
 * Created by jprim on 16-May-17.
 */

public class RandomMotivationalMessages {

    private static List<String> motivationalMessages;
    private static Random randomGenerator;

    static {
        motivationalMessages=new ArrayList<String>();
        randomGenerator = new Random();
        motivationalMessages.add("Stay hungry!");
        motivationalMessages.add("Goals, Goals, Goals!");
        motivationalMessages.add("Make It Fun!");
        motivationalMessages.add("Remember who you're doing it for!");
        motivationalMessages.add("No pain, no gain!");
        motivationalMessages.add("Keep going!");
        motivationalMessages.add("Every workout counts!");
        motivationalMessages.add("Attitude is everything!");
        motivationalMessages.add("Push yourself!");
        motivationalMessages.add("Give it your best!");
        motivationalMessages.add("Make the most of your workout!");
        motivationalMessages.add("Go hard or go home!");
        motivationalMessages.add("Lift big, get big!");
        motivationalMessages.add(" Think Positive!");
        motivationalMessages.add("Forget Fear!");
        motivationalMessages.add("Winners train, losers complain!");
        motivationalMessages.add("Stay Focused!");
        motivationalMessages.add("Test Yourself!");

        motivationalMessages.add("Education is important,but BIG BICEPS are importanter!");
        motivationalMessages.add("Education is important,but BIG BICEPS are importanter!");
        motivationalMessages.add("Education is important,but BIG BICEPS are importanter!");
        motivationalMessages.add("Education is important,but BIG BICEPS are importanter!");
        motivationalMessages.add("Education is important,but BIG BICEPS are importanter!");


    }

    public static String getMotiavationalMessage() {
        int index = randomGenerator.nextInt(motivationalMessages.size());
        return motivationalMessages.get(index);
    }

}
